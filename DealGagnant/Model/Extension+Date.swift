//
//  Extension+Date.swift
//  DealGagnant
//
//  Created by Fabrice M. on 14/03/2022.
//

import Foundation

extension Date {
    
    func dateToString() -> String {
        let formatter = DateFormatter()
        
        formatter.dateStyle = .medium
        formatter.timeStyle = .short
        formatter.locale = Locale(identifier: "fr_FR")
        
        return formatter.string(from: self)
    }
}
