//
//  Anchor.swift
//  DealGagnant
//
//  Created by Fabrice M. on 15/03/2022.
//

import UIKit

// MARK: - Anchor
enum Anchor {
    case top
    case trailing
    case bottom
    case leading
    case centerX
    case centerY
    case witdh
    case height
    case right
    case left
    case topMargin
    case trailingMargin
    case bottomMargin
    case leadingMargin
    
    func attribute() -> NSLayoutConstraint.Attribute {
        switch self {
        case .top:
            return NSLayoutConstraint.Attribute.top
        case .trailing:
            return NSLayoutConstraint.Attribute.trailing
        case .bottom:
            return NSLayoutConstraint.Attribute.bottom
        case .leading:
            return NSLayoutConstraint.Attribute.leading
        case .centerX:
            return NSLayoutConstraint.Attribute.centerX
        case .centerY:
            return NSLayoutConstraint.Attribute.centerY
        case .witdh:
            return NSLayoutConstraint.Attribute.width
        case .height:
            return NSLayoutConstraint.Attribute.height
        case .right:
            return NSLayoutConstraint.Attribute.right
        case .left:
            return NSLayoutConstraint.Attribute.left
        case .topMargin:
            return NSLayoutConstraint.Attribute.topMargin
        case .trailingMargin:
            return NSLayoutConstraint.Attribute.trailingMargin
        case .bottomMargin:
            return NSLayoutConstraint.Attribute.bottomMargin
        case .leadingMargin:
            return NSLayoutConstraint.Attribute.leadingMargin
        }
    }
}
