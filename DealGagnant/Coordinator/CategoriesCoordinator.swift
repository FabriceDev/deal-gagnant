//
//  CategoriesCoordinator.swift
//  DealGagnant
//
//  Created by Fabrice M. on 13/03/2022.
//

import UIKit


class CategoriesCoordinator: BaseCoordinator {
    
    private var categories: Categories?
    
    init(categories: Categories) {
        self.categories = categories
    }
    
    override func start() {
        guard categories != nil else { return }
        let viewModel = CategoriesViewModel(coordinator: self, categories: categories!)
        viewModel.appCoordinatorDelegate = self.parentCoordinator as? AppCoordinatorDelegate
        let controller = CategoriesViewController(viewModel: viewModel)
        navigationController.pushViewController(controller, animated: true)
    }
    
    func segueToListItemsVC() {
        navigationController.popViewController(animated: true)
    }
}
