//
//  Coordinator.swift
//  DealGagnant
//
//  Created by Fabrice M. on 12/03/2022.
//

import UIKit

protocol Coordinator: AnyObject {
    var navigationController: UINavigationController { get set }
    var parentCoordinator: Coordinator? { get set }
    
    func start()
    func start(coordinator: Coordinator)
    func finish(coordinator: Coordinator)
}

class BaseCoordinator: Coordinator {
    
    var navigationController = UINavigationController()
    var parentCoordinator: Coordinator?
    
    private(set) var childCoordinators: [Coordinator] = []
    
    func start() {
        fatalError("Error: This method need to be overriden before use")
    }
    
    func start(coordinator: Coordinator) {
        childCoordinators.append(coordinator)
        coordinator.parentCoordinator = self
        coordinator.start()
    }
    
    func finish(coordinator: Coordinator) {
        if let index = childCoordinators.firstIndex(where: { $0 === coordinator }) {
            childCoordinators.remove(at: index)
        }
    }
}
