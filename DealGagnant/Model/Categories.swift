//
//  Categories.swift
//  DealGagnant
//
//  Created by Fabrice M. on 13/03/2022.
//

import UIKit

// MARK: - Category
struct Category: Decodable {
    let id: Int
    let name: String
    
    func getCategoryImage() -> UIImage? {
        switch self.id {
        case 1:
            return UIImage(systemName: "car.fill")
        case 2:
            return UIImage(systemName: "tshirt.fill")
        case 3:
            return UIImage(systemName: "hammer.fill")
        case 4:
            return UIImage(systemName: "lightbulb.fill")
        case 5:
            return UIImage(systemName: "gamecontroller.fill")
        case 6:
            return UIImage(systemName: "house.fill")
        case 7:
            return UIImage(systemName: "book.fill")
        case 8:
            return UIImage(systemName: "tv.fill")
        case 9:
            return UIImage(systemName: "wrench.fill")
        case 10:
            return UIImage(systemName: "tortoise.fill")
        case 11:
            return UIImage(systemName: "face.smiling.fill")
        default:
            return nil
        }
    }
}

// MARK: - Categories
typealias Categories = [Category]
