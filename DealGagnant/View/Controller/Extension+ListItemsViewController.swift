//
//  Extension+ListItemsViewController.swift
//  DealGagnant
//
//  Created by Fabrice M. on 16/03/2022.
//

import UIKit

extension ListItemsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listItemsCollection.setEdgesClearGradient()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listItemsVM.getListItemsCount()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.listItemsVM.didSelectItem(indexPath: indexPath.item)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: IntroductionCollectionReusableView.identifier, for: indexPath) as? IntroductionCollectionReusableView {
            return header
        }
        return UICollectionReusableView()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ItemsCollectionViewCell.identifier, for: indexPath) as? ItemsCollectionViewCell {
            
            let itemVM = self.listItemsVM.getItem(indexPath: indexPath.item)
            cell.setCell(item: itemVM)
            return cell
        }
        return UICollectionViewCell()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let cell = UIDevice().userInterfaceIdiom == .pad ? 2 : 1
        
        guard let layout = collectionViewLayout as? UICollectionViewFlowLayout else { return CGSize.zero }
        
        layout.minimumLineSpacing = 16
        layout.minimumInteritemSpacing = 20
        
        let total = layout.sectionInset.left + layout.sectionInset.right + (layout.minimumInteritemSpacing * CGFloat(cell - 1))
        
        let size = Int((collectionView.bounds.width - total) / CGFloat(cell))
        
        let height = Int(Double(size)/2.5)
        return CGSize(width: size, height: height)
    }
}
