//
//  Extension+ItemsCollectionViewCell.swift
//  DealGagnant
//
//  Created by Fabrice M. on 15/03/2022.
//

import UIKit

extension ItemsCollectionViewCell {
    
    func setupViews() {
        self.contentView.backgroundColor = .white
        self.setCornerRadius(view: self.contentView)
        self.setShadow(view: self.contentView)
        
        self.contentView.addSubviews([itemsImageView, stackViewTop])
        self.itemsImageView.addSubview(itemsEmergency)
        
        self.itemsImageView.equalToConstraints([.top, .bottom, .leading, .witdh],
                                               views: [self.contentView, self.contentView, self.contentView, nil],
                                               viewsConstants: [0, 0, 0, self.contentView.frame.width / 3])
        
        stackViewBottom.addArrangedSubviews([itemsPrice, itemsDate])
        stackViewMiddle.addArrangedSubviews([itemsCategory, stackViewBottom])
        stackViewTop.addArrangedSubviews([itemsName, stackViewMiddle])
        
        self.stackViewTop.equalToConstraints([.top, .trailing, .bottom, .leading],
                                               views: [self.contentView, self.contentView, self.contentView, self.itemsImageView],
                                               [.top, .trailing, .bottom, .trailing],
                                               viewsConstants: [8, -8, -10, 9])
        
        self.itemsEmergency.equalToConstraints([.bottom, .leading],
                                               views: [self.contentView, self.contentView],
                                               viewsConstants: [-8, 8])
    }
    
    func setCell(item: ItemViewModel) {
        self.itemsName.text = item.getName()
        self.itemsCategory.text = item.getCategory()
        self.itemsPrice.text = item.getPrice()
        self.itemsDate.text = item.getDate()
        self.itemsEmergency.isHidden = !item.getStatus()
        
        Task{
            let image = await item.getImage(small: true)
            DispatchQueue.main.async {
                self.itemsImageView.image = image ?? UIImage(named: "image_not_available")
            }
        }
    }
}
