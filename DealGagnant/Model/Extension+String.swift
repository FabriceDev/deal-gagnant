//
//  Extension+String.swift
//  DealGagnant
//
//  Created by Fabrice M. on 14/03/2022.
//

import Foundation

extension String {
    
    func stringToDate() -> Date? {
        let formatter = DateFormatter()
        
        formatter.locale = Locale.current
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZ"
        
        return formatter.date(from: self)
    }
}
