//
//  ItemViewModel.swift
//  DealGagnant
//
//  Created by Fabrice M. on 15/03/2022.
//

import UIKit

class ItemViewModel {
    
    private let item: Item
    private let categoryName: String
    
    init(item: Item, categoryName: String){
        self.item = item
        self.categoryName = categoryName
    }
    
    func getId() -> String {
        return String(self.item.id)
    }
    
    func getName() -> String {
        return self.item.title
    }
    
    func getImage(small: Bool) async -> UIImage? {
        do {
            return try await self.item.fetchItemsImage(small: small)
        } catch {
            print(error.localizedDescription)
        }
        return nil
    }
    
    func getCategory() -> String {
        return self.categoryName
    }
    
    func getPrice() -> String {
        guard let currency = Locale(identifier: "fr_FR").currencySymbol else { return "" }
        return String(self.item.price)+" "+currency
    }
    
    func getDate() -> String {
        guard let date = self.item.creationDate.stringToDate() else { return ""}
        return date.dateToString()
    }
    
    func getDescription() -> String {
        return self.item.itemsDescription
    }
    
    func getStatus() -> Bool {
        return self.item.isUrgent
    }
}
