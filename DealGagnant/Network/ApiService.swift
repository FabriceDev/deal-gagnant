//
//  ApiHelper.swift
//  DealGagnant
//
//  Created by Fabrice M. on 12/03/2022.
//

import UIKit

protocol ApiServiceProtocol {
    func fetchItemsFromApi() async throws -> Items
    func fetchCategoriesFromApi() async throws -> Categories
}

class ApiService: ApiServiceProtocol {
    
    func fetchItemsFromApi() async throws -> Items {
        guard let url = URL(string: apiRoot+apiDetails) else { fatalError("URL Invalid") }
        let (data, _) = try await URLSession.shared.data(from: url)
        let list = try JSONDecoder().decode(Items.self, from: data)
        
        return list
    }
    
    func fetchCategoriesFromApi() async throws -> Categories {
        guard let url = URL(string: apiRoot+apiCategories) else { fatalError("URL Invalid") }
        
        let (data, _) = try await URLSession.shared.data(from: url)
        let list = try JSONDecoder().decode(Categories.self, from: data)
        
        return list
    }
}
