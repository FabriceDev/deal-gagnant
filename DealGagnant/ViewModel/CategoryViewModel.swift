//
//  CategoryViewModel.swift
//  DealGagnant
//
//  Created by Fabrice M. on 15/03/2022.
//

import Foundation
import UIKit

class CategoryViewModel {
    
    private let category: Category
    
    init(category: Category){
        self.category = category
    }
    
    func getId() -> Int {
        return self.category.id
    }
    
    func getName() -> String {
        return self.category.name
    }
    
    func getImage() -> UIImage? {
        return self.category.getCategoryImage()
    }
}
