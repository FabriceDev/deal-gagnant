//
//  ItemsCollectionViewCell.swift
//  DealGagnant
//
//  Created by Fabrice M. on 12/03/2022.
//

import UIKit

class ItemsCollectionViewCell: UICollectionViewCell {
    
    static var identifier: String = "ItemsCell"
    
    let itemsEmergency: EmergencyView = {
        let emergency = EmergencyView()
        return emergency
    }()
    
    let itemsName: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        label.numberOfLines = 2
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.75
        label.textColor = UIColor.black
        return label
    }()
    
    let itemsPrice: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.font = UIFont.preferredFont(forTextStyle: .title3)
        return label
    }()
    
    let itemsCategory: UILabel = {
        let label = UILabel()
        label.textColor = brownColor
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
        return label
    }()
    
    let itemsDate: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.darkGray
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        return label
    }()
    
    let itemsImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    let stackViewBottom: ItemsStackView = {
        let stackView = ItemsStackView()
        stackView.spacing = 2
        return stackView
    }()
    
    let stackViewMiddle: ItemsStackView = {
        let stackView = ItemsStackView()
        stackView.spacing = 10
        return stackView
    }()
    
    let stackViewTop: ItemsStackView = {
        let stackView = ItemsStackView()
        stackView.distribution = .fillProportionally
        return stackView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("Interface Builder is not supported!")
    }
}
