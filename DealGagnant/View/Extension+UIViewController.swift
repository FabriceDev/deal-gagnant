//
//  Extension+UIViewController.swift
//  DealGagnant
//
//  Created by Fabrice M. on 14/03/2022.
//

import UIKit

extension UIViewController {
    private func setupGradient(height: CGFloat, topColor: CGColor, bottomColor: CGColor) ->  CAGradientLayer {
        let gradient: CAGradientLayer = CAGradientLayer()
        
        gradient.colors = [topColor,bottomColor]
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width, height: height)
        return gradient
    }
    
    func setupClearNavigationBar() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
    }
    
    func setupGradientNavigationBar() {
        let gradientView : UIView = {
            let view = UIView()
            return view
        }()
        
        var gradient : CAGradientLayer?
        
        let height : CGFloat = 130
        let color = UIColor.orange.withAlphaComponent(0.6).cgColor
        let clear = UIColor.white.withAlphaComponent(0.0).cgColor
        
        gradient = setupGradient(height: height, topColor: color, bottomColor: clear)
        view.addSubview(gradientView)
        gradientView.equalToConstraints([.top, .left], views: [self.view, self.view])
        
        guard let g = gradient else { return }
        gradientView.layer.insertSublayer(g, at: 0)
    }
}
