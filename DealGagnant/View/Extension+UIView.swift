//
//  Extension+UIView.swift
//  DealGagnant
//
//  Created by Fabrice M. on 15/03/2022.
//

import UIKit

extension UIView {
    
    func setCornerRadius(view: UIView? = nil) {
        let v = view != nil ? view! : self
        v.layer.cornerRadius = 10.0
        v.layer.borderWidth = 0.15
        v.layer.borderColor = greenColor.cgColor
        v.layer.masksToBounds = true
    }
    
    func setShadow(view: UIView) {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 1.5, height: 1.5)
        self.layer.shadowRadius = 1.0
        self.layer.shadowOpacity = 0.2
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: view.layer.cornerRadius).cgPath
    }

    func addSubviews(_ subviews: [UIView]) {
        for subview in subviews {
            self.addSubview(subview)
        }
    }
    
    func equalToConstraints(_ anchors: [Anchor], views: [UIView?], _ viewsAnchors: [Anchor] = [], viewsConstants: [CGFloat] = [] ) {
        
        if anchors.count == views.count {
            self.translatesAutoresizingMaskIntoConstraints = false
            
            for i in 0..<anchors.count {
                NSLayoutConstraint(item: self,
                                   attribute: anchors[i].attribute(),
                                   relatedBy: NSLayoutConstraint.Relation.equal,
                                   toItem: views[i],
                                   attribute: viewsAnchors.isEmpty ? anchors[i].attribute() : viewsAnchors[i].attribute(),
                                   multiplier: 1,
                                   constant: viewsConstants.isEmpty ? 0 : viewsConstants[i]).isActive = true
            }
        }
    }
    
    func setEdgesClearGradient() {
        let coverView = GradientView(frame: self.bounds)
        
        if let coverLayer = coverView.layer as? CAGradientLayer {
            coverLayer.colors = [UIColor.white.withAlphaComponent(0).cgColor, UIColor.white.withAlphaComponent(0.3).cgColor, UIColor.white.cgColor, UIColor.white.cgColor, UIColor.white.withAlphaComponent(0.8).cgColor, UIColor.white.withAlphaComponent(0).cgColor]
            coverLayer.locations = [0.0, 0.01, 0.02, 0.98, 0.99, 1.0]
            coverLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
            coverLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
            
            self.mask = coverView
        }
    }
}
