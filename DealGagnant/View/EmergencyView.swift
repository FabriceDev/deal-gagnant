//
//  EmergencyView.swift
//  DealGagnant
//
//  Created by Fabrice M. on 14/03/2022.
//

import UIKit

class EmergencyView: UIView {
    
    let emergencyLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.red
        label.text = "Urgent"
        
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.red.cgColor
        self.layer.cornerRadius = 6
        self.backgroundColor = .white

        self.addSubview(emergencyLabel)
        
        self.emergencyLabel.equalToConstraints([.centerX, .centerY], views: [self, self])
        self.equalToConstraints([.witdh, .height], views: [nil, nil], viewsConstants: [55, 20])
    }
}
