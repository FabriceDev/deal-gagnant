//
//  ListItemsCoordinator.swift
//  DealGagnant
//
//  Created by Fabrice M. on 12/03/2022.
//

import UIKit


class ListItemsCoordinator: BaseCoordinator {
    
    override func start() {
        let viewModel = ListItemsViewModel()
        viewModel.appCoordinatorDelegate = self.parentCoordinator as? AppCoordinatorDelegate
        let controller = ListItemsViewController(viewModel: viewModel)
        navigationController.pushViewController(controller, animated: true)
    }
}
