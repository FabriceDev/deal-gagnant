//
//  ListItemsViewModel.swift
//  DealGagnant
//
//  Created by Fabrice M. on 12/03/2022.
//

import UIKit

class ListItemsViewModel {
    
    private let apiService: ApiServiceProtocol
    
    private var items: Items = Items()
    private var itemsFiltered: Items = Items()
    private var categories: Categories = Categories()
    private var selectedId: Int?
    
    weak var appCoordinatorDelegate: AppCoordinatorDelegate?
    
    init(apiService: ApiServiceProtocol = ApiService()) {
        self.apiService = apiService
    }
    
    func setNavigationBarTitle() -> String {
        return self.selectedId != nil ? categories[self.selectedId!].name : "Deal Gagnant"
    }
    
    func fetchData() async {
        do {
            var list = try await apiService.fetchItemsFromApi()
            self.items = list.sortedItems()
            self.categories = try await apiService.fetchCategoriesFromApi()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func isFiltered() {
        if self.selectedId != nil {
            self.itemsFiltered = self.items.filteredItems(category: self.selectedId!)
            
        }
    }
    
    func getListItemsCount() -> Int {
        return self.selectedId != nil ? self.itemsFiltered.count : self.items.count
    }
    
    func getItem(indexPath: Int) -> ItemViewModel {
        let item = self.selectedId != nil ? self.itemsFiltered[indexPath] : self.items[indexPath]
        let index = self.selectedId != nil ? self.itemsFiltered[indexPath].categoryID-1 : self.items[indexPath].categoryID-1
        return ItemViewModel(item: item, categoryName: self.categories[index].name)
    }
    
    func segueToCategoriesVC() {
        self.appCoordinatorDelegate?.segueToCategoriesVC(categories: self.categories)
    } 
    
    func didSelectItem(indexPath: Int) {
        let itemVM = self.getItem(indexPath: indexPath)
        self.appCoordinatorDelegate?.didSelectItem(item: itemVM)
    }
    
    func didChangeCategoryIndex() {
        self.selectedId = self.appCoordinatorDelegate?.categoryIndex
    }
}
