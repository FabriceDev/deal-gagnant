//
//  Constants.swift
//  DealGagnant
//
//  Created by Fabrice M. on 12/03/2022.
//

import UIKit

let apiRoot = "https://raw.githubusercontent.com/leboncoin/paperclip/master"
let apiDetails = "/listing.json"
let apiCategories = "/categories.json"

let greenColor = UIColor.init(_colorLiteralRed: 0/255, green: 102/255, blue: 51/255, alpha: 1)
let brownColor = UIColor.init(_colorLiteralRed: 153/255, green: 76/255, blue: 0/255, alpha: 1)
