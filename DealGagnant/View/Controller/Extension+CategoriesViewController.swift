//
//  Extension+CategoriesViewController.swift
//  DealGagnant
//
//  Created by Fabrice M. on 16/03/2022.
//

import UIKit

extension CategoriesViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categoriesVM.getCategoriesCount()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.categoriesVM.didSelectCategoryIndex(indexPath: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = categoriesTableView.dequeueReusableCell(withIdentifier: CategoriesTableViewCell.identifier, for: indexPath) as? CategoriesTableViewCell {

            let categoryVM = self.categoriesVM.getCategory(indexPath: indexPath.row)
            cell.setCell(category: categoryVM)
            return cell
        }
        return UITableViewCell()
    }
}
