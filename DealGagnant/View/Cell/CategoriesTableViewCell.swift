//
//  CategoriesTableViewCell.swift
//  DealGagnant
//
//  Created by Fabrice M. on 13/03/2022.
//

import UIKit

class CategoriesTableViewCell: UITableViewCell {
    
    static var identifier: String = "categoriesCell"
    
    private let categoriesImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = brownColor
        return imageView
    }()
    
    private let categoriesName: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
        label.textColor = UIColor.black
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: CategoriesTableViewCell.identifier)
        
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.contentView.backgroundColor = .clear
    }
    
    private func setupViews() {
        self.contentView.addSubviews([categoriesImage, categoriesName])
        
        self.contentView.backgroundColor = .white
        self.accessoryType = .disclosureIndicator
        self.selectionStyle = .gray
        
        self.categoriesImage.equalToConstraints([.top, .leading, .centerY, .witdh], views: [self.contentView, self.contentView, self.contentView, nil], viewsConstants: [8, 20, 0, 20])
        
        self.categoriesName.equalToConstraints([.leading, .centerY], views: [self.categoriesImage, self.contentView], [.trailing, .centerY], viewsConstants: [10, 0])
    }
    
    func setCell(category: CategoryViewModel) {
        self.categoriesName.text = category.getName()
        self.categoriesImage.image = category.getImage()
    }
}
