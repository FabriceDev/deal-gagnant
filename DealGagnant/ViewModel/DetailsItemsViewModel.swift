//
//  DetailsItemsViewModel.swift
//  DealGagnant
//
//  Created by Fabrice M. on 14/03/2022.
//

import UIKit

class DetailsItemsViewModel {
    
    private let apiService: ApiServiceProtocol
    
    private weak var detailsItemsCoordinator: DetailsItemsCoordinator!
    
    weak var appCoordinatorDelegate: AppCoordinatorDelegate?
    weak var itemVM: ItemViewModel!
    
    init(coordinator: DetailsItemsCoordinator, item: ItemViewModel, apiService: ApiServiceProtocol = ApiService()) {
        self.detailsItemsCoordinator = coordinator
        self.itemVM = item
        self.apiService = apiService
    }
    
    func getItemId() -> String {
        return "Article N° "+self.itemVM.getId()
    }
    
    func removeChildCoordinator() {
        self.appCoordinatorDelegate?.finishChildCoordinator(coordinator: detailsItemsCoordinator)
    }
}
