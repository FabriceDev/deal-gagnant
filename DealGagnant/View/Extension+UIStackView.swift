//
//  Extension+UIStackView.swift
//  DealGagnant
//
//  Created by Fabrice M. on 15/03/2022.
//

import UIKit

extension UIStackView {
    
    func addArrangedSubviews(_ subviews: [UIView]) {
        
        for subview in subviews {
            self.addArrangedSubview(subview)
        }
    }
}
