//
//  DetailsItemsViewController.swift
//  DealGagnant
//
//  Created by Fabrice M. on 13/03/2022.
//

import UIKit

class DetailsItemsViewController: UIViewController, UIScrollViewDelegate {
    
    var detailsItemsVM: DetailsItemsViewModel!
    
    let itemsName: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        label.numberOfLines = 0
        label.textColor = UIColor.black
        return label
    }()
    
    let itemsPrice: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.font = UIFont.preferredFont(forTextStyle: .title3)
        return label
    }()
    
    let itemsCategory: UILabel = {
        let label = UILabel()
        label.textColor = brownColor
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
        return label
    }()
    
    let itemsDescription: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
        return label
    }()
    
    let itemsDate: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.darkGray
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        return label
    }()
    
    let itemsImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleToFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    let itemsScrollView: UIScrollView = {
        let scrollView = UIScrollView()
        return scrollView
    }()
    
    let itemsEmergency: EmergencyView = {
        let emergency = EmergencyView()
        return emergency
    }()
    
    let itemsContentView: UIView = {
        let view = UIView()
        return view
    }()
    
    init(viewModel: DetailsItemsViewModel) {
        super.init(nibName: nil, bundle: nil)
        self.detailsItemsVM = viewModel
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor(patternImage: UIImage(named:"white_bg")!)
        
        self.navigationItem.title = self.detailsItemsVM.getItemId()
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationController?.navigationBar.tintColor = greenColor
        self.setupClearNavigationBar()
        self.setupGradientNavigationBar()
        
        setupViews()
        setupConstraints()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.detailsItemsVM.removeChildCoordinator()
    }
}
