//
//  GradientView.swift
//  DealGagnant
//
//  Created by Fabrice M. on 15/03/2022.
//

import UIKit

class GradientView: UIView {
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
}
