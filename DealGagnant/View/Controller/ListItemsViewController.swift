//
//  ListItemsViewController.swift
//  DealGagnant
//
//  Created by Fabrice M. on 12/03/2022.
//

import UIKit

class ListItemsViewController: UIViewController {
    
    var listItemsCollection: UICollectionView!
    var listItemsVM: ListItemsViewModel!
    
    init(viewModel: ListItemsViewModel) {
        super.init(nibName: nil, bundle: nil)
        self.listItemsVM = viewModel
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollection()
        
        listItemsCollection.dataSource = self
        listItemsCollection.delegate = self
        
        listItemsCollection.register(ItemsCollectionViewCell.self, forCellWithReuseIdentifier: ItemsCollectionViewCell.identifier)
        listItemsCollection.register(IntroductionCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: IntroductionCollectionReusableView.identifier)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "slider.horizontal.3"), style: .plain, target: self, action: #selector(performCategoriesSegue))
        self.navigationItem.rightBarButtonItem?.tintColor = greenColor
        self.setupClearNavigationBar()
        self.setupGradientNavigationBar()
        
        Task {
            await self.listItemsVM.fetchData()
            self.listItemsCollection.reloadData()
        }
    }
    
    @objc func performCategoriesSegue() {
        self.listItemsVM.segueToCategoriesVC()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.listItemsVM.didChangeCategoryIndex()
        self.listItemsVM.isFiltered()
        
        self.listItemsCollection.reloadData()
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.title = self.listItemsVM.setNavigationBarTitle()
    }
    
    func setupCollection() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 50, left: 5, bottom: 5, right: 5)
        layout.headerReferenceSize = CGSize(width: 100, height: 20)
        
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        self.view.addSubview(collection)
        collection.showsVerticalScrollIndicator = false
        collection.backgroundColor = UIColor.clear
        self.view.backgroundColor = UIColor(patternImage: UIImage(named:"white_bg")!)
        
        collection.equalToConstraints([.top, .bottom, .centerX, .witdh],
                                      views: [self.view, self.view, self.view, nil],
                                      [.topMargin, .bottomMargin, .centerX, .witdh],
                                      viewsConstants: [16, -16, 0, self.view.frame.width-32])
        self.listItemsCollection = collection
    }
}
