//
//  CategoriesViewController.swift
//  DealGagnant
//
//  Created by Fabrice M. on 12/03/2022.
//

import UIKit

class CategoriesViewController: UIViewController {
    
    var categoriesTableView: UITableView!
    var categoriesVM: CategoriesViewModel!
    
    init(viewModel: CategoriesViewModel) {
        super.init(nibName: nil, bundle: nil)
        self.categoriesVM = viewModel
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named:"white_bg")!)
        
        setupTable()
        
        self.categoriesTableView.delegate = self
        self.categoriesTableView.dataSource = self
        self.categoriesTableView.register(CategoriesTableViewCell.self, forCellReuseIdentifier: CategoriesTableViewCell.identifier)
        
        self.navigationItem.title = self.categoriesVM.setNavigationBarTitle()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Reset", style: .plain, target: self, action: #selector(resetButtonPressed))
        self.navigationItem.rightBarButtonItem?.tintColor = greenColor
        self.navigationController?.navigationBar.tintColor = .red
        self.setupClearNavigationBar()
        self.setupGradientNavigationBar()
    }
                                                                 
    @objc func resetButtonPressed() {
        self.categoriesVM.resetButtonPressed()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.categoriesVM.removeChildCoordinator()
    }
    
    func setupTable() {
        self.categoriesTableView = UITableView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        self.categoriesTableView.rowHeight = 48
        self.view.addSubview(self.categoriesTableView)
        
        self.categoriesTableView.showsVerticalScrollIndicator = false
        self.categoriesTableView.setCornerRadius()
        
        self.categoriesTableView.equalToConstraints([.top, .trailing, .leading, .bottom],
                                                    views: Array(repeating: self.view, count: 4),
                                                    [.topMargin, .trailingMargin, .leadingMargin, .bottom],
                                                    viewsConstants: [32, -16, 16, 0])
    }
}
