//
//  AppCoordinator.swift
//  DealGagnant
//
//  Created by Fabrice M. on 12/03/2022.
//

import UIKit

protocol AppCoordinatorDelegate: AnyObject {
    var categoryIndex: Int? { get set }
    
    func segueToCategoriesVC(categories: Categories)
    func didSelectItem(item: ItemViewModel)
    func didSelectCategoryIndex(indexPath: Int?)
    func finishChildCoordinator(coordinator: Coordinator)
}

class AppCoordinator: BaseCoordinator {
    
    private var categoryId: Int?
    
    var window: UIWindow?
    
    init(window: UIWindow?) {
        self.window = window
    }
    
    override func start() {
        guard let window = window else { return }
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
        
        let coordinator = ListItemsCoordinator()
        coordinator.navigationController = navigationController
        start(coordinator: coordinator)
    }
}

extension AppCoordinator: AppCoordinatorDelegate {
    var categoryIndex: Int? {
        get {
            return self.categoryId
        }
        set {
            self.categoryId = newValue
        }
    }
    
    func segueToCategoriesVC(categories: Categories) {
        let coordinator = CategoriesCoordinator(categories: categories)
        coordinator.navigationController = navigationController
        start(coordinator: coordinator)
    }
    
    func didSelectItem(item: ItemViewModel) {
        let coordinator = DetailsItemsCoordinator(item: item)
        coordinator.navigationController = navigationController
        start(coordinator: coordinator)
    }
    
    func didSelectCategoryIndex(indexPath: Int?) {
        self.categoryIndex = indexPath != nil ? indexPath! : indexPath
    }
    
    func finishChildCoordinator(coordinator: Coordinator) {
        finish(coordinator: coordinator)
    }
}
