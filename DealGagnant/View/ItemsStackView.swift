//
//  ItemsStackView.swift
//  DealGagnant
//
//  Created by Fabrice M. on 14/03/2022.
//

import UIKit

class ItemsStackView: UIStackView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        self.axis  = .vertical
        self.alignment = .fill
        self.distribution = .equalCentering
    }
}
