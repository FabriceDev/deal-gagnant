//
//  DetailsItemsCoordinator.swift
//  DealGagnant
//
//  Created by Fabrice M. on 14/03/2022.
//

import UIKit


class DetailsItemsCoordinator: BaseCoordinator {
    
    private var itemVM: ItemViewModel?
    
    init(item: ItemViewModel) {
        self.itemVM = item
    }
    
    override func start() {
        guard itemVM != nil else { return }
        let viewModel = DetailsItemsViewModel(coordinator: self, item: self.itemVM!)
        viewModel.appCoordinatorDelegate = self.parentCoordinator as? AppCoordinatorDelegate
        let controller = DetailsItemsViewController(viewModel: viewModel)
        navigationController.pushViewController(controller, animated: true)
    }
}
