//
//  CategoriesViewModel.swift
//  DealGagnant
//
//  Created by Fabrice M. on 13/03/2022.
//
import Foundation

class CategoriesViewModel {
    
    private weak var categoriesCoordinator: CategoriesCoordinator!
    
    private var categories: Categories = Categories()
    
    weak var appCoordinatorDelegate: AppCoordinatorDelegate?
    
    init(coordinator: CategoriesCoordinator, categories: Categories) {
        self.categoriesCoordinator = coordinator
        self.categories = categories
    }
    
    func setNavigationBarTitle() -> String {
        return "Catégories"
    }
    
    func getCategoriesCount() -> Int {
        return self.categories.count
    }
    
    func getCategory(indexPath: Int) -> CategoryViewModel {
        return CategoryViewModel(category: self.categories[indexPath])
    }
    
    func resetButtonPressed() {
        self.appCoordinatorDelegate?.didSelectCategoryIndex(indexPath: nil)
        self.categoriesCoordinator.segueToListItemsVC()
    }
    
    func didSelectCategoryIndex(indexPath: Int) {
        self.appCoordinatorDelegate?.didSelectCategoryIndex(indexPath: indexPath)
        self.categoriesCoordinator.segueToListItemsVC()
    }
    
    func removeChildCoordinator() {
        self.appCoordinatorDelegate?.finishChildCoordinator(coordinator: categoriesCoordinator)
    }
}
