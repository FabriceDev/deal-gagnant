//
//  Items.swift
//  DealGagnant
//
//  Created by Fabrice M. on 12/03/2022.
//

import UIKit

// MARK: - Item
struct Item: Decodable {
    let id, categoryID: Int
    let title, itemsDescription: String
    let price: Int
    let imagesURL: ImagesURL
    let creationDate: String
    let isUrgent: Bool
    let siret: String?

    enum CodingKeys: String, CodingKey {
        case id
        case categoryID = "category_id"
        case title
        case itemsDescription = "description"
        case price
        case imagesURL = "images_url"
        case creationDate = "creation_date"
        case isUrgent = "is_urgent"
        case siret
    }
    
    func fetchItemsImage(small: Bool) async throws -> UIImage? {
        let urlImage = small ? self.imagesURL.small : self.imagesURL.thumb
        guard let url = urlImage, let urlFromString = URL(string: url) else { return nil }
        
        let (data, _) = try await URLSession.shared.data(from: urlFromString)
        guard let image = UIImage(data: data) else { return nil }
        
        return image
    }
}

// MARK: - ImagesURL
struct ImagesURL: Decodable {
    let small, thumb: String?
}

// MARK: - Items
typealias Items = [Item]

extension Items {
    
    mutating func sortedItems() -> Items {
        let index = self.partition { !$0.isUrgent }
        let isUrgent = self[..<index].sorted { $0.creationDate > $1.creationDate }
        let isntUrgent = self[index...].sorted { $0.creationDate > $1.creationDate }
        
        return isUrgent + isntUrgent
    }
    
    func filteredItems(category: Int) -> Items {
        return self.filter { $0.categoryID == category+1 }
    }
}
