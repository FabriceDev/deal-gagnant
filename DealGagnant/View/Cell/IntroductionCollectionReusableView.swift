//
//  IntroductionCollectionReusableView.swift
//  DealGagnant
//
//  Created by Fabrice M. on 13/03/2022.
//

import UIKit

class IntroductionCollectionReusableView: UICollectionReusableView {
    
    static var identifier: String = "IntroductionCell"
    
    private let introductionLabel: UILabel = {
        let label = UILabel()
        label.text = "Bienvenue,\n 🎁 Que l'offre soit avec vous 🎁"
        label.textAlignment = .center
        label.numberOfLines = 2
        label.font = UIFont.systemFont(ofSize: 16)
        label.adjustsFontSizeToFitWidth = true
        label.textColor = greenColor
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("Interface Builder is not supported!")
    }
    
    private func setupViews() {
        self.addSubview(introductionLabel)
        
        self.introductionLabel.equalToConstraints([.bottom, .centerX], views: [self, self], viewsConstants: [32, 0])
    }
}
