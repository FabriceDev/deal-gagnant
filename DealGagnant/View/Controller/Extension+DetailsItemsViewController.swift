//
//  Extension+DetailsItemsViewController.swift
//  DealGagnant
//
//  Created by Fabrice M. on 15/03/2022.
//

import UIKit

extension DetailsItemsViewController {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.itemsScrollView.setEdgesClearGradient()
    }
    
    func setupViews() {
        self.view.addSubview(self.itemsScrollView)
        self.itemsScrollView.addSubview(self.itemsContentView)
        self.itemsContentView.backgroundColor = .white
        self.itemsScrollView.delegate = self
        self.itemsScrollView.showsVerticalScrollIndicator = false
        
        self.itemsName.text = self.detailsItemsVM.itemVM.getName()
        self.itemsName.numberOfLines = 0
        self.itemsName.textAlignment = .center
        self.itemsContentView.addSubview(self.itemsName)
        
        if self.detailsItemsVM.itemVM.getStatus() {
            self.itemsContentView.addSubview(self.itemsEmergency)
            
            self.itemsEmergency.equalToConstraints([.top, .centerX],
                                              views: [self.itemsName, self.itemsContentView],
                                                   [.bottom, .centerX],
                                              viewsConstants: [8, 0])
        }

        Task {
            let image = await self.detailsItemsVM.itemVM.getImage(small: false)
            DispatchQueue.main.async {
                self.itemsImageView.image = image ?? UIImage(named: "image_not_available")
            }
        }
        self.itemsContentView.addSubview(self.itemsImageView)

        self.itemsCategory.text = self.detailsItemsVM.itemVM.getCategory()
        self.itemsCategory.textAlignment = .center
        self.itemsContentView.addSubview(self.itemsCategory)

        self.itemsPrice.text = self.detailsItemsVM.itemVM.getPrice()
        self.itemsPrice.textAlignment = .center
        self.itemsContentView.addSubview(self.itemsPrice)

        self.itemsDate.text = self.detailsItemsVM.itemVM.getDate()
        self.itemsDate.textAlignment = .center
        self.itemsContentView.addSubview(self.itemsDate)

        self.itemsDescription.text = self.detailsItemsVM.itemVM.getDescription()
        self.itemsDescription.numberOfLines = 0
        self.itemsDescription.textAlignment = .center
        self.itemsContentView.addSubview(self.itemsDescription)
        
        self.itemsImageView.setCornerRadius()
        self.itemsContentView.setCornerRadius()
    }
    
    func setupConstraints() {
        self.itemsScrollView.equalToConstraints([.top, .trailing, .bottom, .leading],
                                                views: [self.view, self.view, self.view, self.view],
                                                [.topMargin, .trailingMargin, .bottomMargin, .leadingMargin],
                                                viewsConstants: [8, -4, 0, 4])

        
        self.itemsContentView.equalToConstraints([.top, .trailing, .bottom, .leading, .centerX],
                                                 views: Array(repeating: self.itemsScrollView, count: 5),
                                                 viewsConstants: [24, 0, -16, 0, 0])
        
        
        self.itemsName.equalToConstraints([.top, .trailing, .leading],
                                          views: Array(repeating: self.itemsContentView, count: 3),
                                          [.topMargin, .trailingMargin, .leadingMargin],
                                          viewsConstants: [32, -4, 4])
        
        
        self.itemsImageView.equalToConstraints([.top, .trailing, .leading, .height],
                                               views: [self.itemsName, self.itemsContentView, self.itemsContentView, nil],
                                               [.bottom, .trailingMargin, .leadingMargin, .height],
                                               viewsConstants: [40, -(UIScreen.main.bounds.width/6), (UIScreen.main.bounds.width/6), UIScreen.main.bounds.height/4])
        
        
        self.itemsCategory.equalToConstraints([.top, .trailing, .leading],
                                              views: [self.itemsImageView, self.itemsContentView, self.itemsContentView],
                                              [.bottom, .trailingMargin, .leadingMargin],
                                              viewsConstants: [16, -16, 16])
        
        
        self.itemsPrice.equalToConstraints([.top, .trailing, .leading],
                                              views: [self.itemsCategory, self.itemsContentView, self.itemsContentView],
                                              [.bottom, .trailingMargin, .leadingMargin],
                                              viewsConstants: [24, -16, 16])

        
        self.itemsDate.equalToConstraints([.top, .trailing, .leading],
                                              views: [self.itemsPrice, self.itemsContentView, self.itemsContentView],
                                              [.bottom, .trailingMargin, .leadingMargin],
                                              viewsConstants: [48, -32, 32])
        
        
        self.itemsDescription.equalToConstraints([.top, .trailing, .bottom, .leading],
                                                 views: [self.itemsDate, self.itemsContentView,
                                                         self.itemsContentView, self.itemsContentView],
                                                 [.bottom, .trailingMargin, .bottomMargin, .leadingMargin],
                                                 viewsConstants: [16, -24, -24, 32])
    }
}
